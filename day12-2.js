// USE NODE
var fs = require('fs');
var a =  JSON.parse(fs.readFileSync('inputs/day12.txt').toString());

function parse(thing) {
	
	var total = 0;

	if (!Array.isArray(thing) && typeof thing === 'object') {

		// OBJECT
		// Loop over obj to see if any of the keys are red
		var hasRed = false;

		Object.keys(thing).forEach(function(key) {

			if (thing[key] === 'red') {

				hasRed = true;

			}

		});

		if (!hasRed) {

			Object.keys(thing).forEach(function(key) {

				total+= parse(thing[key]);

			});

		}

	} else if (Array.isArray(thing) && typeof thing === 'object') {

		// ARRAY
		thing.forEach(function(element) {

			total+= parse(element);

		});

	} else if (typeof thing === 'number') {

		// NUMBER
		total+= thing;

	} else {

		// STRING
		return 0;

	}

	return total;
	
}

console.log(parse(a));
