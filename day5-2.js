// USE NODE

var fs = require('fs');
var list =  fs.readFileSync('inputs/day5.txt').toString().split('\n');
var niceList = [];

function pairAppearsTwice(string) {

	for (var i = 0; i < (string.length - 1); i++) {

		twoChars = string[i] + string[i + 1];

		// See if those 2 chars appear anywhere else in the sting
		regex = new RegExp(twoChars, 'g');
		matches = string.match(regex);

		if (matches.length > 1) {

			return true;

		}

	}

	return false;

}

function isNice(string) {

	// It contains a pair of any two letters that appears at least twice in the string without overlapping
	//like xyxy (xy) or aabcdefgaa (aa), but not like aaa (aa, but it overlaps).
	if (!pairAppearsTwice(string)) {

		return false;

	}

	// It contains at least one letter which repeats with exactly one letter between them, like xyx, abcdefeghi (efe), or even aaa.
	if (!/([a-z])[a-z]\1/.test(string)) {

		return false;

	}

	return true;

}

for (var i = 0; i < list.length; i++) {

	if (isNice(list[i])) {

		niceList.push(list[i]);

	}

}

console.log("The nice list contains: " + niceList.length);