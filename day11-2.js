var alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];

function increment(str) {

	// Split the string into letters
	letters = str.split('');

	// Loop over each of the letters backwards
	for (var i = letters.length - 1; i >= 0; i--) {

		var c = letters[i];

		if (c !== 'z') {

			letters[i] = alphabet[alphabet.indexOf(c) + 1];

			return letters.join('');

		} else {

			letters.splice(-1, 1);

			return increment(letters.join('')) + 'a';

		}

	}

}

function hasRunOfCharacters(str, num) {

	var result = false;

	// Split the string into letters
	var letters = str.split('');
	letterIDs = letters.map(function(letter) {
		return alphabet.indexOf(letter);
	});

	// Count the runs
	var runs = 1;
	letterIDs.reduce(function(a, b) {

		if (a === (b - 1)) {
			runs++;
		} else {
			runs = 1;
		}

		if (runs === num) {

			result = true;

		}

		return b;

	});

	return result;

}

function numberOfDifferentPairs(str) {

	var matches = str.match(/(\w)\1/g) || [];

	if (matches.length > 0) {

		matches.filter(function(value, index, self) { 
			return self.indexOf(value) === index;
		});

	}

	return matches.length;

}

function validPassword(pw) {

	// Contains i, o or l
	if (/i|o|l/g.test(pw)) {

		return false;

	} 
	// Doesn't contain more than 2 pairs of letters
	else if (numberOfDifferentPairs(pw) < 2) {

		return false;

	}
	// Doesn't have run of characters
	else if (!hasRunOfCharacters(pw, 3)) {

		return false;

	}
	else {

		return true;

	}

}

input = 'hepxxyzz';

do {

	input = increment(input);

} while (validPassword(input) === false);

console.log(input);