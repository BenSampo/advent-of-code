// USE NODE

var crypto = require('crypto');
var timeStart = Date.now();
var timeEnd;

function lowestHash(key) {

	var i = 0;
	var hash;

	do {

		hash = crypto.createHash('md5').update(key + i).digest('hex');

		if (hash.charAt(0) === '0' && hash.slice(0,6) === '000000') {
			break;
		}

		i+= 1;

	} while (true);


	timeEnd = Date.now();
	console.log("Done in: " + (timeEnd - timeStart) + 'ms');
	return [i, hash];

}

console.log(lowestHash('bgvyzdsv'));