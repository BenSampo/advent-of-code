// USE NODE

var fs = require('fs');
var list =  fs.readFileSync('inputs/day5.txt').toString().split('\n');
var niceList = [];

function vowelCount(string) {

	var vowels = ['a', 'e', 'i', 'o', 'u'];
	var count = 0;
	var regex;

	for (var i = 0; i < vowels.length; i++) {

		regex = new RegExp(vowels[i], 'g');
		count+= (string.match(regex) || []).length;

	}

	return count;

}

function characterAppearsTwice(string) {

	return /([a-z])\1/g.test(string);

}

function isNice(string) {

	// It contains at least three vowels (aeiou only), like aei, xazegov, or aeiouaeiouaeiou.
	if (vowelCount(string) < 3) {
		return false;
	}

	// It contains at least one letter that appears twice in a row, like xx, abcdde (dd), or aabbccdd (aa, bb, cc, or dd).
	if (!characterAppearsTwice(string)) {
		return false;
	}

	// It does not contain the strings ab, cd, pq, or xy, even if they are part of one of the other requirements.
	if (/ab|cd|pq|xy/g.test(string)) {
		return false;
	}

	console.log(string);

	return true;

}

for (var i = 0; i < list.length; i++) {

	if (isNice(list[i])) {

		niceList.push(list[i]);

	}

}

console.log("The nice list contains: " + niceList.length);