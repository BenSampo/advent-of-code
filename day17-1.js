// USE NODE
var fs = require('fs');
var containers =  fs.readFileSync('inputs/day17.txt').toString().trim().split('\n').map(function(c) {
	return parseInt(c);
});

function permuations(arr) {

	var permArr = [],
	usedChars = [];

	function permute(input) {
		var i, ch;
		for (i = 0; i < input.length; i++) {
			ch = input.splice(i, 1)[0];
			usedChars.push(ch);
			if (input.length == 0) {
				permArr.push(usedChars.slice());
			}
			permute(input);
			input.splice(i, 0, ch);
			usedChars.pop();
		}
		return permArr;
	}

	return permute(arr);

}

var count = 0;

permuations(containers).forEach(function(possibleCombo) {

	var totalContents = possibleCombo.reduce(function(a, b) {

		return a + b;

	}, 0);

	//console.log(totalContents);

});