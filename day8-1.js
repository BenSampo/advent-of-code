// Use node
var fs = require('fs');
var strings =  fs.readFileSync('inputs/day8.txt').toString().split('\n');

function decode(string) {

	// Remove the double quote from beggining and end of string
	string = string.substring(1, string.length - 1);

	// Replace \\ with \
	string = string.replace(/\\\\/g, "A");

	// Replace \" with "
	string = string.replace(/\\"/g, 'B');

	// Replace \xAA with a
	string = string.replace(/\\x([a-z0-9]{2})/g, 'C');

	return string;

}

var codeLength = 0;
var memLength = 0;

for (var i = 0; i < strings.length; i++) {

	codeLength+= strings[i].length;
	memLength+= decode(strings[i]).length;

}

console.log('Code Length: ' + codeLength);
console.log('Mem Length: ' + memLength);
console.log('Answer: ' + (codeLength - memLength));