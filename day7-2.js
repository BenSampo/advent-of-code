// Use node
var fs = require('fs');
var inputs =  fs.readFileSync('inputs/day7.txt').toString().split('\n');
var results = {b: 3176};

function solve(input) {

	// If input is number, return it
	if (/^[0-9]+$/g.test(input)) {

		return parseInt(input);

	}

	// If it's in the results already...
	if (results[input] !== undefined) {

		// console.log("GET FROM RESULTS");
		return results[input];

	}

	// Search inputs for the line ending '-> x'
	var line = inputs.find(function(element, index, array) {
		if (element.split(' -> ')[1] === input) {
			return element;
		} else {
			return '';
		}
	});


	var command = line.split(' -> ')[0];
	var eq = line.split(' -> ')[1];

	// If command is number, return it
	if (/^[0-9]+$/g.test(command)) {

		results[eq] = parseInt(command);
		return parseInt(command);

	}
	// Just 1 wire (2 letters)
	else if (/^[a-z]{2}$/g.test(command)) {

		result = solve(command);

	}
	// OR
	else if (command.indexOf('OR') !== -1) {

		command = command.split(' ');
		result = solve(command[0]) | solve(command[2]);

	}
	// AND
	else if (command.indexOf('AND') !== -1) {

		command = command.split(' ');
		result = solve(command[0]) & solve(command[2]);

	}
	// RSHIFT
	else if (command.indexOf('RSHIFT') !== -1) {

		command = command.split(' ');
		result = solve(command[0]) >> command[2];

	}
	// LSHIFT
	else if (command.indexOf('LSHIFT') !== -1) {

		command = command.split(' ');
		result = solve(command[0]) << command[2];

	}
	// NOT
	else if (command.indexOf('NOT') !== -1) {

		command = command.split(' ');
		result = ~ solve(command[1]);

	}
	else {

		return;

	}

	results[input] = parseInt(result);
	return result;

}

console.log(solve('a'));