// Use node
var fs = require('fs');
var inputs =  fs.readFileSync('inputs/day6.txt').toString().split('\n');

// Build an empty grid
function buildEmptyGrid(w, h) {

	var grid = [];

	for (var i = 0; i < h; i++) {

		var gridX = [];

			for (var j = 0; j < w; j++) {

				gridX.push(0);

			}

		grid.push(gridX);

	}

	return grid;

}

function lightOn(x, y) {

	grid[x][y]++;	
	
}

function lightOff(x, y) {

	if (grid[x][y] !== 0) {

		grid[x][y]--;	

	}
	
}

function lightToggle(x, y) {

	grid[x][y] = grid[x][y] + 2;
	
}

function lightCount() {

	var count = 0;

	for (var i = 0; i < grid.length; i++) {

		for (var j = 0; j < grid[i].length; j++) {

			count = count + grid[i][j];

		}

	}

	return count;

}

function performInstruction(input) {

	var inputAction;
	var inputSimple;
	var xStart = 0;
	var xEnd = 0;
	var yStart = 0;
	var yEnd = 0;
	var width;
	var height;
	var lightX;
	var lightY;

	if (input.indexOf('turn on') !== -1) {

		inputAction = 'on';

	} else if (input.indexOf('turn off') !== -1) {

		inputAction = 'off';

	} else {

		inputAction = 'toggle';

	}

	inputSimple = input.replace(' through ', ',').replace(/[^0-9\,\:]/g, '').split(',');
	xStart = parseInt(inputSimple[0]);
	xEnd = parseInt(inputSimple[2]);
	yStart = parseInt(inputSimple[1]);
	yEnd = parseInt(inputSimple[3]);
	width = (xEnd - xStart) + 1;
	height = (yEnd - yStart) + 1;

	for (var i = 0; i < height; i++) {

		for (var j = 0; j < width; j++) {

			lightY = yStart + i;
			lightX = xStart + j;

			switch (inputAction) {
				case 'on':
					lightOn(lightX, lightY);
				break;
				case 'off':
					lightOff(lightX, lightY);
				break;
				case 'toggle':
					lightToggle(lightX, lightY);
				break;
			}

		}

	}

}

var grid = buildEmptyGrid(1000, 1000);

for (var i = 0; i < inputs.length; i++) {

	performInstruction(inputs[i]);

}

console.log('There are ' + lightCount() + ' lights on');
