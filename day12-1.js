// USE NODE
var fs = require('fs');
var a =  JSON.parse(fs.readFileSync('inputs/day12.txt').toString());

function parse(thing) {
	
	var total = 0;

	if (!Array.isArray(thing) && typeof thing === 'object') {

		// OBJECT
		Object.keys(thing).forEach(function(key) {

			total+= parse(thing[key]);

		});

	} else if (Array.isArray(thing) && typeof thing === 'object') {

		// ARRAY
		thing.forEach(function(element) {

			total+= parse(element);

		});

	} else if (typeof thing === 'number') {

		// NUMBER
		total+= thing;

	} else {

		// STRING
		return 0;

	}

	return total;
	
}

console.log(parse(a));
