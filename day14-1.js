// USE NODE
var fs = require('fs');
var inputs =  fs.readFileSync('inputs/day14.txt').toString().trim().split('\n');

var Deer = function(name, fs, ft, rt) {
	this.name = name,
	this.flySpeed = fs,
	this.flyTime = ft,
	this.restTime = rt
}

var allDeer = inputs.map(function(input) {

	var regex = /^([a-zA-Z]+) can fly (\d+) km\/s for (\d+) seconds, but then must rest for (\d+) seconds\.$/g;
	var parts = regex.exec(input);

	return new Deer(parts[1], parseInt(parts[2]), parseInt(parts[3]), parseInt(parts[4]));

});

function testFlight(deer, seconds) {

	var fs = deer.flySpeed;
	var ft = deer.flyTime;
	var rt = deer.restTime;
	var d = 0;
	var state = 'unknown';

	// Cycles refers to how many complete times a deer can fly then rest
	var cycles = Math.floor(seconds / (ft + rt));
	var timeLeft = seconds - (cycles * (ft + rt));

	d = (fs * ft) * cycles;

	if (timeLeft > ft) {

		state = 'resting';
		d+= (fs * ft);

	} else {

		d+= (fs * timeLeft);

		state = 'flying';
		
	}

	console.log('After ' + seconds + ' seconds, ' + deer.name + ' has travelled ' + d + ' KM\'s and is ' + state + '.');

	return d;

}

allDeer.forEach(function(deer) {

	testFlight(deer, 2503);
	
});
