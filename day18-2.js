// USE NODE
var fs = require('fs');
var input =  fs.readFileSync('inputs/day18.txt').toString().trim().split('\n');

// Create the initial grid of lights
var grid = [];

input.forEach(function(row) {

	grid.push(row.split(''));

});

function nextGrid(grid) {

	var currentGrid = grid;
	var newGrid = JSON.parse(JSON.stringify(grid));

	currentGrid.forEach(function(row, i) {

		row.forEach(function(light, j) {

			if (!((i === 0 && j === 0) || (i === 0 && j === (row.length - 1)) || (i === (currentGrid.length - 1) && j === 0) || (i === (currentGrid.length - 1) && j === (row.length - 1)))) {

				var neighbours = [];

				if (i > 0) {
					// Top
					neighbours.push(currentGrid[i-1][j]);
					if (j > 0) {
						// Top Left
						neighbours.push(currentGrid[i-1][j-1]);
					}
					if (j < (row.length - 1)) {
						// Top Right
						neighbours.push(currentGrid[i-1][j+1]);
					}
				}

				if (j > 0) {
					// Left
					neighbours.push(currentGrid[i][j-1]);
				}

				if (j < (row.length - 1)) {
					// Right
					neighbours.push(currentGrid[i][j+1]);
				}

				if (i < (currentGrid.length - 1)) {
					// Bottom
					neighbours.push(currentGrid[i+1][j]);
					if (j > 0) {
						// Bottom Left
						neighbours.push(currentGrid[i+1][j-1]);
					}
					if (j < (row.length - 1)) {
						// Bottom Right
						neighbours.push(currentGrid[i+1][j+1]);
					}
				}

				var neighboursOn = neighbours.filter(function(n) {
					if (n === '#') return n;
				}).length;

				// A light which is on stays on when 2 or 3 neighbors are on, and turns off otherwise.
				if (light === '#') {

					if (neighboursOn !== 2 && neighboursOn !== 3) {
						newGrid[i][j] = '.';
					}

				} 
				// A light which is off turns on if exactly 3 neighbors are on, and stays off otherwise.
				else {

					if (neighboursOn === 3) {
						newGrid[i][j] = '#';
					}

				}

			}

		});

	});
	
	return newGrid;

}

for (var i = 0; i < 100; i++) {
	
	grid = nextGrid(grid);
	
}

lightsOn = grid.reduce(function(a, b) {
	return a.concat(b);
}, []).filter(function(l) {
	if (l === '#') return l;
}).length;

console.log(lightsOn);