// USE NODE
var fs = require('fs');
var input =  fs.readFileSync('inputs/day13.txt').toString().trim().split('\n');

function permuations(arr) {

	var permArr = [],
	usedChars = [];

	function permute(input) {
		var i, ch;
		for (i = 0; i < input.length; i++) {
			ch = input.splice(i, 1)[0];
			usedChars.push(ch);
			if (input.length == 0) {
				permArr.push(usedChars.slice());
			}
			permute(input);
			input.splice(i, 0, ch);
			usedChars.pop();
		}
		return permArr;
	}

	return permute(arr);

}

var guests = input.map(function(line) {

	var guest = line.split(' ')[0];

	return guest;

}).filter(function(value, index, self) {
	return self.indexOf(value) === index;
});

function calcHappiness(a, b) {

	var h = 0;

	for (var i = 0; i < input.length; i++) {

		if(input[i].indexOf(a) === 0 && input[i].indexOf(b) > 0) {

			h = parseInt(input[i].match(/\d+/)[0]);

			if (input[i].indexOf('lose') !== -1) {

				h = h * -1;

			}

		}
		
	}

	return h;

}

function calcTableHappiness(guests) {

	var h = 0;

	for (var i = 0; i < guests.length; i++) {

		guest = guests[i];

		if (i === 0) {
			prevGuest = guests[guests.length - 1];
		} else {
			prevGuest = guests[i - 1];
		}

		if (i === (guests.length - 1)) {
			nextGuest = guests[0];
		} else {
			nextGuest = guests[i + 1];
		}

		h+= calcHappiness(guest, prevGuest);
		h+= calcHappiness(guest, nextGuest);

	}

	return h;

}


var possibleSeatings = permuations(guests);
var bestSeatingHappiness = 0;

possibleSeatings.forEach(function(seating) {

	var h = calcTableHappiness(seating);

	if (h > bestSeatingHappiness) {
		bestSeatingHappiness = h;
	}

});

console.log(bestSeatingHappiness);