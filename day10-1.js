function nextLookAndSay(number) {

	var next = [];
	var count = 1;

	// Split into same numbers
	numbers = number.toString().split('');

	for (var i = 0; i < numbers.length; i++) {

		// If current char == next char
		if (numbers[i] === numbers[i + 1]) {

			count++;

		} else {

			next.push(count);
			next.push(numbers[i]);
			count = 1;

		}

	}

	return next.join('');

}

var lookAndSay = '1321131112';

for (var i = 0; i < 40; i++) {

	lookAndSay = nextLookAndSay(lookAndSay);

}

console.log(lookAndSay.length);