
function permuations(arr) {

	var permArr = [],
	usedChars = [];

	function permute(input) {
		var i, ch;
		for (i = 0; i < input.length; i++) {
			ch = input.splice(i, 1)[0];
			usedChars.push(ch);
			if (input.length == 0) {
				permArr.push(usedChars.slice());
			}
			permute(input);
			input.splice(i, 0, ch);
			usedChars.pop();
		}
		return permArr;
	}

	return permute(arr);

}

// Use node
var fs = require('fs');
var input =  fs.readFileSync('inputs/day9.txt').toString();

// distances between locations
var d = input.trim().split('\n').map(function(str) {
	var parts = str.split(' ');
	var strRev = parts[2] + ' to ' + parts[0] + ' = ' + parts[4];
	return [str, strRev]; 
}).reduce(function(a, b) {
	return a.concat(b);
}, []);

// Function to get the distance between 2 locations
function getD(a, b) {

	for (var i = 0; i < d.length; i++) {

		if (d[i].indexOf(a) !== -1 && d[i].indexOf(b) !== -1) {

			return parseInt(d[i].split(' = ')[1]);

		}

	}

	return undefined;

}

// Unique locations
var l = input.split('\n').join(' ').replace(/ to /g, ' ').replace(/ = [0-9]+/g, '').trim().split(' ').filter(function(value, index, self) {
	return self.indexOf(value) === index;
});

// Number of locations
var nol = l.length;

// Possible journeys
var pj = permuations(l);

// Loop over the possible journeys and work out the shortest
var pjd = [];
for (var i = 0; i < pj.length; i++) {

	var journey = pj[i];
	var jd = 0;

	for (var j = 0; j < (journey.length - 1); j++) {

		jd+= getD(journey[j], journey[j + 1]);

	}

	pjd.push(jd);

}

// Sort the journey distances and get the first to find the shortest.
var longest = pjd.sort(function(a, b) {
  return a - b;
})[pjd.length-1];

console.log(longest);