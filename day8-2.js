// Use node
var fs = require('fs');
var strings =  fs.readFileSync('inputs/day8.txt').toString().split('\n');

function encode(string) {

	if (string.length === 0) return '';

	// Replace \ with \\
	string = string.replace(/\\/g, '\\\\');

	// Replace " with \"
	string = string.replace(/"/g, '\\"');

	// Wrap in double quotes
	string = '"' + string + '"';

	return string;

}

var codeLength = 0;
var encodeLength = 0;

for (var i = 0; i < strings.length; i++) {

	codeLength+= strings[i].length;
	encodeLength+= encode(strings[i]).length;

}

console.log('Code Length: ' + codeLength);
console.log('Encode Length: ' + encodeLength);
console.log('Answer: ' + (encodeLength - codeLength));